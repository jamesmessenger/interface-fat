#include <atmel_start.h>
#include <util/delay.h>

void print_string(char s[]);
void run_test(char test);
void init();

int main(void)
{
	
	char c;
	char i;
	/* Initializes MCU, drivers and middleware */

	init();

	while (1)
	{
		
		_delay_ms(10);
		LED_toggle_level();
		c = USART_0_read();
		if (c>='0' && c<='9')
		{
			run_test(atoi(&c));
		}
		else
		print_string("Invalid command\r\n");

	}
	
	
	
	

}

void init()
{
	atmel_start_init();
	_delay_ms(200);
	print_string("\r\n\033[H\033[2JUtonomy Interface Board Test\r\n");
	print_string("Enter a command number to run (1-9) or 0 to reset the application\r\n");

}

void print_string(char s[])
{
	RS485_nRE_set_level(1);
	printf("%s",s);
	_delay_ms(10);
	RS485_nRE_set_level(0);
}

void run_test(char test)
{

	char i2c_read_data;
	
	switch(test)
	{
		case 0:
			init();
			break;
		
		case 1:
			print_string("Command 1: Setting ENCODER_PWR on (P4 pin 8)\r\n");
			ENC_POWER_set_level(1);
			break;
		
		
		
		case 2:
			print_string("Command 2: Check ENC_ERR is LOW\r\n");
			if(ENC_ERR_get_level() == 0)
			{
				print_string("PASS: ENC_ERR (P4 pin 4) is LOW\r\n");
			}
			else
			{
				print_string("FAIL: ENC_ERR (P4 pin 4) is HIGH, expected LOW\r\n");
			}
			break;
			
		case 3:
			print_string("Command 3: Check ENC_ERR is HIGH\r\n");
			if(ENC_ERR_get_level() == 1)
			{
				print_string("PASS: ENC_ERR (P4 pin 4) is HIGH\r\n");
			}
			else
			{
				print_string("FAIL: ENC_ERR (P4 pin 4) is LOW, expected HIGH\r\n");
			}
			break;
			
		case 4:
			print_string("Command 4: Check ENC_WRN is LOW\r\n");
			if(ENC_WRN_get_level() == 0)
			{
				print_string("PASS: ENC_WRN (P4 pin 5) is LOW\r\n");
			}
			else
			{
				print_string("FAIL: ENC_WRN (P4 pin 5) is HIGH, expected LOW\r\n");
			}
			break;
			
		case 5:
			print_string("Command 5: Check ENC_WRN is HIGH\r\n");
			if(ENC_WRN_get_level() == 1)
			{
				print_string("PASS: ENC_WRN (P4 pin 5) is HIGH\r\n");
			}
			else
			{
				print_string("FAIL: ENC_WRN (P4 pin 5) is LOW, expected HIGH\r\n");
			}
			break;
			
		case 6:
			print_string("Command 6: Setting ENC_BAT output HIGH (P4 pin 3)\r\n");
			ENC_BAT_set_level(1);
		break;
		
		case 7:
			print_string("Command 7: Setting ENC_PRE output HIGH (P4 pin 2)\r\n");
			ENC_PRE_set_level(1);
		break;
		
		case 8:
		//todo: handle timeout
		print_string("Command 8: Read 1 byte from I2C address 0x4F\r\n");
		if(I2C_0_test_i2c_master(&i2c_read_data))
		{
			print_string("FAIL: Could not establish communications with slave device\r\n");		
		}
		else if(i2c_read_data == 0x55)
		{
			print_string("PASS: Data received matches expected 0x55\r\n");
		}
		else
		{
			print_string("FAIL: Data does not match expected 0x55\r\n");
		}
		break;

		case 9:
		print_string("Command 9: Setting ACT_ALERT output HIGH (Connector P1 Pin U)\r\n");
		RS485_ALERT_set_level(1);
		break;
		
		
	}
	
}